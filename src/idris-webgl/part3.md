---
title: Preliminary performance test
tags: #Idris, #functional
link_previous: ../part2
link_top: ..
link_next: ../part4
---

I decided to run some preliminary performance tests.
While a spinning triangle is not a very relevant benchmark, the code is still at the stage where I can easily rewrite the entire thing in native JavaScript for an apples-to-apples comparison. If my worries result in seriously slower code we might be able to see some effect at least.

I wanted to measure FPS. To so so I [enabled an FPS counter in the browser](https://cesium.com/blog/2014/12/01/webgl-profiling-tips/) and [disabled vSync](https://stackoverflow.com/a/18062444) to get values above the 60 FPS cap. A restart of the browser was required for changes to take effect.

Note: I'm on Linux (Pop!_OS) running nvidia-driver-455 on a GTX 1660 super GPU and an i5-6600K CPU.

![FPS counter overlay](fps-counter.png)

The spinning triangle got about 287 FPS using native JS and 280 when compiling from Idris (it drops when taking screenshots). Various minifiers and optimizers seemed to either output non-functioning code, make no changes or have no effect on performance.

So there does appear to be some effect on performance, but not so much as to have me worried yet.

Idris still has a big trick to pull: compiling to Web Assembly. Since Idris has a C++ backend, and [emscripten](https://emscripten.org/) exists, there could be a substantial opportunity for performance improvements here. For the moment, this does not seem relevant to test, as the code is mostly calls to WebGL functions. API calls cannot be made from WASM yet so we'd mostly be adding JS-calling overhead. On the other hand, I expect getting that idea to work will require a substantial effort that I'd rather spend doing something else. Once the CPU load gets more substantial this would be an interesting comparison.
